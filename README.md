# AppCivist Notification Service
This service is based on the idea presented in [here](https://www.ibm.com/developerworks/library/wa-notify-app/) 

See the [documentation](documentation.md) to learn how to use it

You can also check the [original code](https://hub.jazz.net/project/kjwillia/notification-service/overview?utm_source=dw&utm_medium=article&utm_content=wa-notify-app&utm_campaign=bluemix)

Original Notes
====================
This software is licensed under the The MIT License (MIT) which is a permissive free software license.  See the associated LICENSE.md for details.

Simple Notoification Service is a server application with a Rest-based interface which allows definition, subscription 
and signaling of Events. When an Event is signaled then all subscribers receive notification containing Event instance 
information specific to the signal.

The service runs as a Node application and employs MongoDb as the backend.

API information on the wiki

### License ###

 This software is provided under a dual license model designed to meet the 
 development and distribution needs of both commercial usage and open source 
 projects.
 
 If you intend to use this software for commercial purposes, contact the project
 members presented in the next section.

### Who do I talk to? ###

* Rafael Angarita: rafael.angarita AT inria.fr (main developer)
* Nikolaos Georgantas nikolaos.georgantas AT inria.fr (designer)
* Valérie Issarny valerie.issarny AT inria.fr (designer)